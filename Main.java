class Main {
    
    /**
     * Main -metode for å teste FireProtectionSystem -classen
     * Det som foregår her simulerer alle "input" til
     * systemet.
     * 
     * @param args
     */
    public static void main(String[] args) {
        // Oppretter et nytt objekt
        FireProtectionSystem system = new FireProtectionSystem();

        // Installere sensorer
        // Lager en array { null, null, null, null }
        Sensor sensor1 = new Sensor("192.168.1.200");
        Sensor sensor2 = new Sensor("192.168.1.200");
        System.out.println(sensor1.equals(sensor2));
        system.installSensor(sensor1, sensor2);

        // Installere alarmer
        Alarm myAlarm = new Alarm();  // Oppretter et nytt Alarm -objekt
        system.installAlarm(myAlarm);

        // Oppdag røyk
        sensor1.setSmokeDetected(true);

        sensor1.setSmokeDetected(false);

    }
}