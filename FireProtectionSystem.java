import java.util.HashSet;

/**
 * Et FireProctectionSystem -objekt har til hensikt å koordinere
 * sensorer og alarmer, slik at alle alarmer er aktivert dersom én
 * eller flere sensorer detekterer røyk.
 * 
 * @author Torstein Strømme
 */
class FireProtectionSystem {
    // Feltvariabler (én kopi av hver av disse for hvert objekt)
    HashSet<Sensor> sensors = new HashSet<>();
    HashSet<Alarm> alarms = new HashSet<>();

    /**
     * Installer en eller flere sensorer i systemet.
     * @param mySensors en sensor som skal installerse
     */
    public void installSensor(Sensor... mySensors) {
        for (Sensor sensor : mySensors) {
            if (sensors.contains(sensor)) {
                System.err.println("The sensor " + sensor + " was already installed");
            }
            else {
                this.sensors.add(sensor);
                sensor.installSystem(this);
            }
        }
    }

    /**
     * Installer en alarm i systemet.
     * @param myAlarm
     */
    public void installAlarm(Alarm myAlarm) {
        this.alarms.add(myAlarm);
    }
    
    /**
     * Gir beskejed til systemet at røyk er oppdaget
     */
    void smokeDetected() {
        for (Alarm alarm : alarms) {
            alarm.setActive(true);
        }
    }

    /**
     * Gir beskjed til systemet at en sensor har sluttet å detektere røyk
     */
    void stoppedDetectingSmoke() {
        if (allSensorsAreOff()) {
            for (Alarm alarm : alarms) {
                alarm.setActive(false);
            }
        }
    }

    /**
     * Sjekker om alle sensorer er av (ikke detekterer røyk).
     * @return <code>true</code> dersom ingen installerte sensorer detekterer røyk,
     * <code>false</code> hvis én eller flere sensorer detekterer røyk.
     */
    private boolean allSensorsAreOff() {
        for (Sensor sensor : this.sensors) {
            if (sensor.smokeDetected) {
                return false;
            }
        }
        return true;
    }


}
